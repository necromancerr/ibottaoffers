//
//  IbottaOffersTests.swift
//  IbottaOffersTests
//
//  Created by Pratish Karmacharya on 5/13/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import XCTest
import Moya

@testable import IbottaOffers

class IbottaOffersTests: XCTestCase {
    var networkManager: NetworkManager?
    var offersVC: OffersViewController?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        self.networkManager = NetworkManager(provider: MoyaProvider<IBottaAPI>(stubClosure: MoyaProvider.immediatelyStub))
        offersVC = OffersViewController()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testOffersViewController() {
        offersVC?.loadViewIfNeeded()
        XCTAssertTrue(offersVC?.offersCollectionView?.dataSource is OffersViewController, "collection view's datasource shoud be OffersViewController")
        XCTAssertTrue(offersVC?.offersCollectionView?.delegate is OffersViewController, "collection view's delegate shoud be OffersViewController")

        let cell:OfferCollectionViewCell = offersVC?.collectionView(offersVC!.offersCollectionView!, cellForItemAt: IndexPath(row:0, section:0)) as! OfferCollectionViewCell
        XCTAssertEqual(cell.titleLabel?.text, offersVC?.offers[0].currentValue, "price of first item didn't match")
        XCTAssertEqual(cell.subTitleLabel?.text, offersVC?.offers[0].name, "name of first item didn't match")
        XCTAssertEqual(cell.favoriteButton!.isFavorite, offersVC?.offers[0].favorite, "favorite selection of first item didn't match")
    }

    func testOfferDetailViewController() {
        offersVC?.loadViewIfNeeded()
        let offerDetail = OfferDetailViewController()
        XCTAssertNil(offerDetail.offer)
        XCTAssertNil(offerDetail.networkManager)
        XCTAssertNil(offerDetail.favoriteHandler)
        
        /// Check Offer objects
        offerDetail.offer = offersVC?.offers[0]
        XCTAssertEqual(offerDetail.offer?.name, offersVC!.offers[0].name, "name of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.offer?.currentValue, offersVC!.offers[0].currentValue, "price of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.offer?.description, offersVC!.offers[0].description, "description of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.offer?.terms, offersVC!.offers[0].terms, "terms of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.offer?.url, offersVC!.offers[0].url, "url of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.offer?.favorite, offersVC!.offers[0].favorite, "favorite of selected item and offer detail didn't match")
        
        
    }
    
    func testOfferDetailUIElements() {
        offersVC?.loadViewIfNeeded()
        let offerDetail = OfferDetailViewController()
        offerDetail.offer = offersVC?.offers[0]
        offerDetail.loadViewIfNeeded()
        
        /// Check UI Elements
        XCTAssertEqual(offerDetail.nameLabel?.text, offersVC!.offers[0].name, "name of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.priceLabel?.text, offersVC!.offers[0].currentValue, "price of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.descriptionLabel?.text, offersVC!.offers[0].description, "description of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.termsLabel?.text, offersVC!.offers[0].terms, "terms of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.offer?.url, offersVC!.offers[0].url, "url of selected item and offer detail didn't match")
        XCTAssertEqual(offerDetail.favoriteButton?.isFavorite, offersVC!.offers[0].favorite, "favorite of selected item and offer detail didn't match")
    }
    
    func testFavoriteButton() {
        offersVC?.loadViewIfNeeded()
        let offerDetail = OfferDetailViewController()
        offerDetail.offer = offersVC?.offers[0]
        offerDetail.loadViewIfNeeded()
        XCTAssertTrue((offerDetail.favoriteButton!.isFavorite!), "favorite should be true")
        
        /// Specify completion handler for favorite button
        offerDetail.favoriteHandler = { (isFavorite) in
            self.offersVC?.offers[0] = offerDetail.offer!
            self.offersVC?.offersCollectionView!.reloadItems(at: [IndexPath(row: 0, section: 0)])
        }
        
        /// Simulate favorite tapped
        offerDetail.favoriteTapped(sender: offerDetail.favoriteButton!)
        
        /// Favorite should be false now
        XCTAssertFalse((offerDetail.favoriteButton!.isFavorite!), "favorite should be false")
        XCTAssertEqual(offerDetail.favoriteButton?.isFavorite, offersVC!.offers[0].favorite, "favorite of selected item and offer detail didn't match")
        
        /// Check favorite on the collection view now. it should also be false
        let cell:OfferCollectionViewCell = offersVC?.collectionView(offersVC!.offersCollectionView!, cellForItemAt: IndexPath(row:0, section:0)) as! OfferCollectionViewCell
        XCTAssertFalse(cell.favoriteButton!.isFavorite!, "favorite should be false")
        XCTAssertEqual(cell.favoriteButton!.isFavorite, offersVC?.offers[0].favorite, "favorite selection of first item didn't match")
    }
}
