//
//  Offer.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/13/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

/// Model for Offers
struct Offer {
    let id: String?
    let name: String?
    let terms: String?
    let url: URL?
    let description: String?
    let currentValue: String?
    var favorite: Bool?
}

/// Build from JSON
extension Offer: Decodable {
    enum OfferCodingKeys: String, CodingKey {
        case id
        case name
        case terms
        case url
        case description
        case currentValue = "current_value"
        case favorite
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OfferCodingKeys.self)
        
        id = try container.decodeIfPresent(String.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        terms = try container.decodeIfPresent(String.self, forKey: .terms)
        url = try container.decodeIfPresent(URL.self, forKey: .url)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        currentValue = try container.decodeIfPresent(String.self, forKey: .currentValue)
        do {
            favorite = try container.decode(Bool.self, forKey: .favorite)
        } catch {
            favorite = false
        }
    }
}
