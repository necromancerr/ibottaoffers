//
//  OfferDetailViewController.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/14/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class OfferDetailViewController: UIViewController {
    
    var contentView: UIScrollView?
    var offerImageView: UIImageView?
    var priceLabel: UILabel?
    var nameLabel: UILabel?
    var detailLabel: UILabel?
    var descriptionLabel: UILabel?
    var conditionLabel: UILabel?
    var termsLabel: UILabel?
    var favoriteButton: IBottaFavoriteButton?
    
    var offer: Offer?
    var networkManager: NetworkManager?
    var favoriteHandler:((Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = false
        
        setupViews()
        loadData()
        setupAutoLayout()
        title = offer?.name
    }
    
    private func setupViews() {
        contentView = UIScrollView(frame: self.view.bounds)
        contentView?.contentSize = CGSize(width: self.view.bounds.width, height: self.view.bounds.height)
        contentView?.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(contentView!)
        
        offerImageView = UIImageView(frame: .zero)
        offerImageView?.translatesAutoresizingMaskIntoConstraints = false
        offerImageView?.contentMode = .scaleAspectFit
        contentView!.addSubview(offerImageView!)
        
        priceLabel = UILabel()
        priceLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 14)
        priceLabel?.textColor = UIColor.darkGray
        priceLabel?.translatesAutoresizingMaskIntoConstraints = false
        contentView!.addSubview(priceLabel!)
        
        nameLabel = UILabel()
        nameLabel?.font = UIFont(name: "AvenirNext-Regular", size: 13)
        nameLabel?.textColor = UIColor.gray
        nameLabel?.translatesAutoresizingMaskIntoConstraints = false
        contentView!.addSubview(nameLabel!)
        
        detailLabel = UILabel()
        detailLabel?.text = "Detail"
        detailLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        detailLabel?.textColor = UIColor.darkGray
        detailLabel?.translatesAutoresizingMaskIntoConstraints = false
        contentView!.addSubview(detailLabel!)
        
        descriptionLabel = UILabel()
        descriptionLabel?.numberOfLines = 0
        descriptionLabel?.font = UIFont(name: "AvenirNext-Regular", size: 13)
        descriptionLabel?.textColor = UIColor.gray
        descriptionLabel?.translatesAutoresizingMaskIntoConstraints = false
        contentView!.addSubview(descriptionLabel!)
        
        conditionLabel = UILabel()
        conditionLabel?.text = "Terms & Condition"
        conditionLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        conditionLabel?.textColor = UIColor.darkGray
        conditionLabel?.translatesAutoresizingMaskIntoConstraints = false
        contentView!.addSubview(conditionLabel!)
        
        termsLabel = UILabel()
        termsLabel?.numberOfLines = 0
        termsLabel?.font = UIFont(name: "AvenirNext-Regular", size: 13)
        termsLabel?.textColor = UIColor.gray
        termsLabel?.translatesAutoresizingMaskIntoConstraints = false
        contentView!.addSubview(termsLabel!)
        
        favoriteButton = IBottaFavoriteButton(type: .roundedRect)
        favoriteButton?.translatesAutoresizingMaskIntoConstraints = false
        favoriteButton?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(favoriteTapped)))
        contentView!.addSubview(favoriteButton!)
    }
    
    @objc func favoriteTapped(sender: UIButton) {
        offer?.favorite?.toggle()
        favoriteButton?.isFavorite = offer?.favorite
        favoriteHandler?((offer?.favorite)!)
    }
    
    private func loadData() {
        if let imageUrl = offer?.url {
            networkManager?.getImage(url: imageUrl, completion: { (image, error) in
                self.offerImageView?.image = image
            })
        }
        favoriteButton?.isFavorite = offer?.favorite
        priceLabel?.text = offer?.currentValue
        nameLabel?.text = offer?.name
        descriptionLabel?.text = offer?.description
        termsLabel?.text = offer?.terms
    }
    
    private func setupAutoLayout() {
        contentView?.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        contentView?.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        contentView?.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        contentView?.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        
        offerImageView?.topAnchor.constraint(equalTo: contentView!.topAnchor, constant: 8).isActive = true
        offerImageView?.centerXAnchor.constraint(equalTo: self.contentView!.centerXAnchor).isActive = true
        offerImageView?.widthAnchor.constraint(equalTo: self.contentView!.widthAnchor, constant: -32).isActive = true
        var constraint = offerImageView?.heightAnchor.constraint(equalTo: offerImageView!.widthAnchor, multiplier: 0.6)
        constraint?.priority = UILayoutPriority(100)
        constraint?.isActive = true
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            constraint = offerImageView?.heightAnchor.constraint(lessThanOrEqualToConstant: 300)
        } else {
            constraint = offerImageView?.heightAnchor.constraint(lessThanOrEqualToConstant: 150)
        }
        constraint?.priority = UILayoutPriority(1000)
        constraint?.isActive = true
        
        priceLabel?.topAnchor.constraint(equalTo: self.offerImageView!.bottomAnchor, constant: 8).isActive = true
        priceLabel?.leadingAnchor.constraint(equalTo: self.offerImageView!.leadingAnchor).isActive = true
        priceLabel?.trailingAnchor.constraint(equalTo: self.offerImageView!.trailingAnchor).isActive = true
        
        nameLabel?.topAnchor.constraint(equalTo: self.priceLabel!.bottomAnchor, constant: 8).isActive = true
        nameLabel?.leadingAnchor.constraint(equalTo: self.offerImageView!.leadingAnchor).isActive = true
        nameLabel?.trailingAnchor.constraint(equalTo: self.offerImageView!.trailingAnchor).isActive = true
        
        detailLabel?.topAnchor.constraint(equalTo: self.nameLabel!.bottomAnchor, constant: 16).isActive = true
        detailLabel?.leadingAnchor.constraint(equalTo: self.offerImageView!.leadingAnchor, constant: 0).isActive = true
        detailLabel?.trailingAnchor.constraint(equalTo: self.offerImageView!.trailingAnchor, constant: 0).isActive = true
        
        descriptionLabel?.topAnchor.constraint(equalTo: self.detailLabel!.bottomAnchor, constant: 4).isActive = true
        descriptionLabel?.leadingAnchor.constraint(equalTo: self.offerImageView!.leadingAnchor).isActive = true
        descriptionLabel?.trailingAnchor.constraint(equalTo: self.offerImageView!.trailingAnchor).isActive = true
        
        conditionLabel?.topAnchor.constraint(equalTo: self.descriptionLabel!.bottomAnchor, constant: 16).isActive = true
        conditionLabel?.leadingAnchor.constraint(equalTo: self.offerImageView!.leadingAnchor, constant: 0).isActive = true
        conditionLabel?.trailingAnchor.constraint(equalTo: self.offerImageView!.trailingAnchor, constant: 0).isActive = true
        
        termsLabel?.topAnchor.constraint(equalTo: self.conditionLabel!.bottomAnchor, constant: 4).isActive = true
        termsLabel?.leadingAnchor.constraint(equalTo: self.offerImageView!.leadingAnchor).isActive = true
        termsLabel?.trailingAnchor.constraint(equalTo: self.offerImageView!.trailingAnchor).isActive = true
        
        favoriteButton?.topAnchor.constraint(equalTo: offerImageView!.bottomAnchor, constant: 8).isActive = true
        favoriteButton?.trailingAnchor.constraint(equalTo: offerImageView!.trailingAnchor, constant: 0).isActive = true
        let favSize:CGFloat
        if UIDevice.current.userInterfaceIdiom == .pad {
            favSize = 40.0
        } else {
            favSize = 20.0
        }
        favoriteButton?.widthAnchor.constraint(equalToConstant: favSize).isActive = true
        favoriteButton?.heightAnchor.constraint(equalToConstant: favSize).isActive = true
    }

}
