//
//  UIColorExtension.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/16/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init?(hex string: String) {
        let hexString = string.replacingOccurrences(of: "#", with: "")
        
        var hexInt:UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.scanHexInt32(&hexInt)
        let r = CGFloat((hexInt & 0xFF0000) >> 16) / 255
        let g = CGFloat((hexInt & 0x00FF00) >> 8) / 255
        let b = CGFloat((hexInt & 0x0000FF) >> 0) / 255
        
        self.init(red:r, green:g, blue:b, alpha:1.0)
    }
}
