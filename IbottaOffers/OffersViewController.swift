//
//  ViewController.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/13/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit
import Moya


class OffersViewController: UIViewController{
    let cellIdentifier = "OfferCell"

    let top:CGFloat = 24.0
    let bottom:CGFloat = 24.0
    let right:CGFloat = 12.0
    let left:CGFloat = 12.0
    
    let rowInset:CGFloat = 24.0
    let itemInset:CGFloat = 8.0
    
    let itemsPerRow = 2

    var offersCollectionView: UICollectionView?
    var networkManager: NetworkManager?
    var imageNetworkManager: NetworkManager?
    var offers:[Offer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Offers"
        
        setupCollectionView()
        setupAutoLayout()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        
        // Padding around the child content
        // Values are provided based on the mockup
        layout.sectionInset = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        // Vertical space between two rows in a UICollectionView
        layout.minimumLineSpacing = rowInset // 24 Based on the mockup
        layout.minimumInteritemSpacing = itemInset

        offersCollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        offersCollectionView?.register(OfferCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        offersCollectionView?.backgroundColor = UIColor.white
        offersCollectionView?.delegate = self
        offersCollectionView?.dataSource = self
        offersCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(offersCollectionView!)
    }
    
    private func setupAutoLayout() {
        offersCollectionView?.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        offersCollectionView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        offersCollectionView?.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        offersCollectionView?.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
    }
    
    func loadData() {
        networkManager = NetworkManager(provider: MoyaProvider<IBottaAPI>(stubClosure: MoyaProvider.immediatelyStub))
        networkManager?.getOffers(completion: { (results, error) in
            if let offers = results {
                self.offers = offers
                self.offersCollectionView?.reloadData()
            }
        })
    }
}

extension OffersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! OfferCollectionViewCell
        cell.titleLabel?.text = offers[indexPath.item].currentValue
        cell.subTitleLabel?.text = offers[indexPath.item].name
        if let imageURL = offers[indexPath.item].url {
            networkManager?.getImage(url: imageURL, completion: { (result, error) in
                cell.imageView?.image = result
            })
        } else {
            cell.imageView?.image = nil
        }
        cell.favoriteButton?.isFavorite = offers[indexPath.item].favorite
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let referenceWidth = (self.view.bounds.width - left - right - itemInset) / CGFloat(itemsPerRow)
        let referenceHeight: CGFloat = referenceWidth * 0.6 + 50
        return CGSize(width: referenceWidth, height: referenceHeight)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var offer = offers[indexPath.item]
        let offerDetailViewController = OfferDetailViewController()
        offerDetailViewController.offer = offer
        offerDetailViewController.networkManager = networkManager
        offerDetailViewController.modalPresentationStyle = .overFullScreen
        offerDetailViewController.favoriteHandler = { favorite in
            offer.favorite = favorite
            self.offers[indexPath.item] = offer
            collectionView.reloadItems(at: [indexPath])
        }
        self.navigationController?.pushViewController(offerDetailViewController, animated: true)
    }
}

