//
//  OfferCollectionViewCell.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/13/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class OfferCollectionViewCell: UICollectionViewCell {
    var imageBackgroundView: UIView?
    var imageView: UIImageView?
    var titleLabel: UILabel?
    var subTitleLabel: UILabel?
    var favoriteButton: IBottaFavoriteButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.translatesAutoresizingMaskIntoConstraints = false;
        self.contentView.autoresizingMask.insert(.flexibleHeight)
        self.contentView.autoresizingMask.insert(.flexibleWidth)

        setupViews()
        
        setupAutoLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        // Setup Image Background View
        imageBackgroundView = UIView(frame: .zero)
        imageBackgroundView?.backgroundColor = UIColor(hex: "#F1F2F5")
        imageBackgroundView?.layer.cornerRadius = 5
        self.contentView.addSubview(imageBackgroundView!)
        
        // Setup ImageView
        imageView = UIImageView(frame: .zero)
        imageView?.contentMode = .scaleAspectFit
        imageBackgroundView?.addSubview(imageView!)
        
        // Setup title label
        titleLabel = UILabel()
        titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 12)
        titleLabel?.textColor = UIColor.darkGray
        self.contentView.addSubview(titleLabel!)
        
        // Setup subtitle label
        subTitleLabel = UILabel()
        subTitleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 11)
        subTitleLabel?.textColor = UIColor.gray
        self.contentView.addSubview(subTitleLabel!)
        
        favoriteButton = IBottaFavoriteButton(type: .roundedRect)
        self.contentView.addSubview(favoriteButton!)
    }
    
    private func setupAutoLayout() {
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        imageBackgroundView?.translatesAutoresizingMaskIntoConstraints  = false
        imageBackgroundView?.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        imageBackgroundView?.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        imageBackgroundView?.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        imageBackgroundView?.heightAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.6).isActive = true

        imageView?.translatesAutoresizingMaskIntoConstraints = false
        imageView?.leadingAnchor.constraint(equalTo: self.imageBackgroundView!.leadingAnchor, constant: 6).isActive = true
        imageView?.trailingAnchor.constraint(equalTo: self.imageBackgroundView!.trailingAnchor, constant: -6).isActive = true
        imageView?.topAnchor.constraint(equalTo: self.imageBackgroundView!.topAnchor, constant: 6).isActive = true
        imageView?.bottomAnchor.constraint(equalTo: self.imageBackgroundView!.bottomAnchor, constant: -6).isActive = true

        titleLabel?.translatesAutoresizingMaskIntoConstraints = false
        titleLabel?.topAnchor.constraint(equalTo: self.imageBackgroundView!.bottomAnchor, constant: 8).isActive = true
        titleLabel?.trailingAnchor.constraint(equalTo: self.imageBackgroundView!.trailingAnchor, constant: 0).isActive = true
        titleLabel?.leadingAnchor.constraint(equalTo: self.imageBackgroundView!.leadingAnchor, constant: 0).isActive = true

        subTitleLabel?.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel?.topAnchor.constraint(equalTo: self.titleLabel!.bottomAnchor, constant: 8).isActive = true
        subTitleLabel?.trailingAnchor.constraint(equalTo: self.imageBackgroundView!.trailingAnchor, constant: 3).isActive = true
        subTitleLabel?.leadingAnchor.constraint(equalTo: self.imageBackgroundView!.leadingAnchor, constant: 0).isActive = true

        favoriteButton?.translatesAutoresizingMaskIntoConstraints = false
        favoriteButton?.trailingAnchor.constraint(equalTo: imageBackgroundView!.trailingAnchor, constant: -6).isActive = true
        favoriteButton?.bottomAnchor.constraint(equalTo: imageBackgroundView!.bottomAnchor, constant: -6).isActive = true
        
        let favSize:CGFloat
        if UIDevice.current.userInterfaceIdiom == .pad {
            favSize = 40.0
        } else {
            favSize = 20.0
        }
        favoriteButton?.widthAnchor.constraint(equalToConstant: favSize).isActive = true
        favoriteButton?.heightAnchor.constraint(equalToConstant: favSize).isActive = true
    }
}
