//
//  IBottaFavoriteButton.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/15/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class IBottaFavoriteButton: UIButton {

    var isFavorite:Bool? {
        didSet {
            if isFavorite! {
                self.tintColor = UIColor.orange
                pulsate()
            } else {
                self.tintColor = UIColor.lightGray
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setImage(UIImage(named: "star"), for: .normal)
        tintColor = UIColor.gray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.fromValue = 0
        pulse.toValue = 1.0
        self.layer.add(pulse, forKey: "pulse")
    }
    
    
    
}
