//
//  IBottaAPI.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/13/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation
import Moya

/// List of operations we can perform on IBotta API
enum IBottaAPI {
    case offers
    case image(url: URL)
}

/// IBottaAPI must conform to TargetType
/// Add all the required methods to it fully conforms to TargetType
extension IBottaAPI: TargetType {
    var baseURL: URL {
        switch self {
        case .offers:
            /// Change to prod api for getting data from server
            return URL(string: "http://api.tvmaze.com/")!
        case .image(let imageUrl):
            return imageUrl
        }
    }
    
    /// Path of api call. returning empty for this scenario
    var path: String {
        switch self {
        case .offers, .image:
            return ""
            
        }
    }
    
    /// HTTP Method
    var method: Moya.Method {
        switch self {
        case .offers, .image:
            return .get
        }
    }
    
    /// Sample data for testing.
    var sampleData: Data {
        switch self {
        case .offers:
            let url = Bundle.main.url(forResource: "data/Offers", withExtension: "json")!
            return try! Data(contentsOf: url)
        case .image:
            return Data()
        }
    }
    
    /// Add request parameters here
    var task: Task {
        switch self {
        case .offers, .image:
            return .requestPlain
        }
    }
    
    /// Headers for the rest api.
    var headers: [String : String]? {
        switch self {
        case .offers, .image:
            return ["Content-type": "application/json"]
        }
    }
}
