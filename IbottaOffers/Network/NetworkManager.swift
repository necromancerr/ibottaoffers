//
//  NetworkManager.swift
//  IbottaOffers
//
//  Created by Pratish Karmacharya on 5/13/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation
import Moya

/// Object to handle all network calls
struct NetworkManager {
    let provider: MoyaProvider<IBottaAPI>
    
    init(provider: MoyaProvider<IBottaAPI> = MoyaProvider<IBottaAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])) {
        self.provider = provider
    }
    
    /// Get all offer from the API
    func getOffers(completion: @escaping([Offer]?, Error?) -> Void) {
        provider.request(.offers) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode([Offer].self, from: response.data)
                    completion(results, nil)
                } catch let err {
                    completion(nil, err)
                }
            case let .failure(error):
                completion(nil, error)
            }
        }
    }
    
    
    /// Get image from the provided url.
    /// - parameter url: Url of the image to get
    func getImage(url: URL, completion: @escaping(Image?, Error?) -> Void) {
        let imageCache = (UIApplication.shared.delegate as! AppDelegate).imageCache
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage, nil)
        } else {
            /// Typically you don't need to create a new provider here. For sample app, I had to create a new provider
            /// replace this by struct level provider for real data
            let imageProvider = MoyaProvider<IBottaAPI>()
            imageProvider.request(.image(url: url)) { result in
                switch result {
                case let .success(response):
                    let image = Image.init(data: response.data)
                    imageCache.setObject(image!, forKey: url.absoluteString as NSString)
                    completion(image, nil)
                case let .failure(error):
                    completion(nil, error)
                }
            }
        }
    }
}
